



def calculate_BMI(weight, length):
    mas_index = weight/(length*length)
    mas_index = mas_index * 10000
    return mas_index


def get_string_from_index(mas_index): # pragma: no cover
    if mas_index >= 40:
        string = "У вас ожирение 3-ей степени."
    elif (mas_index >= 35) & (mas_index < 40):
        string = "У вас ожирение 2-ой степени."
    elif (mas_index >= 30) & (mas_index < 35):
        string = "У вас ожирение 1-ой степени."
    elif (mas_index >= 25) & (mas_index < 30):
        string = "У вас избыточная масса тела (предшествует ожирению)."
    elif (mas_index >= 18.5) & (mas_index < 25):
        string = "У вас нормальный вес."
    elif (mas_index >= 16) & (mas_index < 18.5):
        string = "У вас недостаточная масса тела (дефицит)."
    elif (mas_index > 0) & (mas_index < 16):
        string = "У вас выраженный дефицит массы тела." 
    else:
        string = "Неверное значение. Как прокачаться до джуна в Си?"
    return string


if __name__ == "__main__": # pragma: no cover
    index = calculate_BMI(int(input("Введите значение веса в килограммах: ")), int(input("Введите значение роста в сантиметрах: ")))
    print()
    print("Ваш ИМТ равен:", index)
    print(get_string_from_index(index))