from project import calculate_BMI

def test_fatweight_3():
    assert calculate_BMI(130, 178) == 41.030172957959856


def test_fatweight_2():
    assert calculate_BMI(120, 178) == 37.87400580734756


def test_fatweight_1():
    assert calculate_BMI(80, 160) == 31.25


def test_fatweight_horizon():
    assert calculate_BMI(90, 178) == 28.40550435551067


def test_normal_weight():
    assert calculate_BMI(70, 178) == 22.093170054286073


def test_defweight_1():
    assert calculate_BMI(55, 180) == 16.97530864197531


def test_defweight_2():
    assert calculate_BMI(50, 180) == 15.432098765432098